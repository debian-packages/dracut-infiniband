#!/bin/bash

check() {
    return 0 # Include the dracut module in the initramfs.
}

install() {
    inst_hook pre-udev 39 "$moddir/infiniband.sh"
}

installkernel() {
    instmods mlx4_core
    instmods mlx5_ib
    instmods mlx4_ib
    instmods ib_ipoib
    instmods ib_core
    instmods ib_mad
    instmods ib_sa
    instmods ib_addr
    instmods iw_cm
    instmods ib_cm
    instmods rdma_cm
    instmods rdma_ucm
    instmods ib_ucm
    instmods ib_uverbs
    instmods ib_umad
    instmods rdma_cm
    instmods ib_cm
    instmods iw_cm
    instmods ib_sa
    instmods ib_mad
    instmods ipv6
    instmods rds
    instmods ib_srp
    instmods ib_iser
    instmods rdma_cm
    instmods ib_uverbs
    instmods rdma_ucm
    instmods mst_pciconf
    instmods mst_pci
    instmods eth_ipoib
    instmods mlx4_core
    instmods mlx4_en
    instmods mlx5_core
}
